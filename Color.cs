﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerracenCore
{
    public struct Color
    {
        public float R, G, B, A;

        public Color(float red, float green, float blue, float alpha)
        {
            this.R = red;
            this.G = green;
            this.B = blue;
            this.A = alpha;
        }

        public Tuple4D Vector4D
        {
            get { return new Tuple4D(R, G, B, A); }
            set { R = value.X; G = value.Y; B = value.Z; A = value.W; }
        }

        internal OpenGL.Vector4 Vector4
        {
            get { return new OpenGL.Vector4(R, G, B, A); }
            set { R = value.x; G = value.y; B = value.z; A = value.w; }
        }

        public override string ToString()
        {
            return "Red: " + R + ", Green: " + G + ", Blue: " + B + ", Alpha: " + A;
        }

        internal static Color Null  { get { return new Color(-1, -1, -1, -1);   } set { } }
        public static Color Black   { get { return new Color(0, 0, 0, 0);       } set { } }
        public static Color White   { get { return new Color(1, 1, 1, 1);       } set { } }
        public static Color Red     { get { return new Color(1, 0, 0, 1);       } set { } }
        public static Color Green   { get { return new Color(0, 1, 0, 1);       } set { } }
        public static Color Blue    { get { return new Color(0, 0, 1, 1);       } set { } }
        public static Color Yellow  { get { return new Color(1, 1, 0, 1);       } set { } }
        public static Color Purple  { get { return new Color(1, 0, 1, 1);       } set { } }
        public static Color Aqua    { get { return new Color(0, 1, 1, 1);       } set { } }
    }
}
