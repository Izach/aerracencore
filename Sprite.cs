﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerracenCore
{
    public class Sprite
    {
        public Texture Texture;
        public Tuple2D Position;
        public int X { get { return Position.X; } set { Position.X = value; } }
        public int Y { get { return Position.Y; } set { Position.Y = value; } }
        public int Width { get { return Texture.iTexture.Size.Width; } set { } }
        public int Height { get { return Texture.iTexture.Size.Height; } set { } }
        public Tuple2D Origin;

        /// <summary>
        /// Destroys the spirtes texture;
        /// </summary>
        public void Destroy()
        {
            Texture.Destroy();
        }

        internal void Initialize()
        {
            Origin = Align.GetPosition(Alignment.Center, Width, Height);
        }

        public Sprite(int x, int y, Texture texture) { Position = new Tuple2D(x, y); Texture = texture; Initialize(); }
        public Sprite(Texture texture) { Position = new Tuple2D(0, 0); Texture = texture; Initialize(); }
        public Sprite(Tuple2D position, Texture texture) { Position = position; Texture = texture; Initialize(); }
        
    }
}
