﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerracenCore
{
    public class Polygon
    {
        public Tuple2D[] Points;

        public Polygon(params Tuple2D[] points)
        {
            Points = points;
        }

        public Polygon(params int[] points)
        {
            int length = points.GetLength(0);
            if ((length % 2) == 0)
            {
                Points = new Tuple2D[length / 2];
                for (int i = 0; i < length / 2; i++)
                {
                    Points[i] = new Tuple2D(points[i * 2], points[(i * 2) + 1]);
                }
            }
            else throw new Exception("Not a multiple of two.");
        }

        public Line Line
        {
            get
            {
                if (Points.GetLength(0) >= 2) return new Line(Points[0].X, Points[0].Y, Points[1].X, Points[1].Y);
                else throw new Exception("The polygon is to small for a line.");
            }
            set
            {
                if (Points.GetLength(0) >= 2)
                {
                    Points[0] = new Tuple2D(value.X1, value.Y1);
                    Points[1] = new Tuple2D(value.X2, value.Y2);
                }
                else throw new Exception("The polygon is to small for a line.");
            }
        }

        public Triangle Triangle
        {
            get
            {
                if (Points.GetLength(0) >= 3)
                {
                    return new Triangle(Points[0].X, Points[0].Y, Points[1].X, Points[1].Y, Points[2].X, Points[2].Y);
                }
                else throw new Exception("The polygon is to small for a triangle.");
            }
            set
            {
                if (Points.GetLength(0) >= 3)
                {
                    Points[0] = new Tuple2D(value.X1, value.Y1);
                    Points[1] = new Tuple2D(value.X2, value.Y2);
                    Points[2] = new Tuple2D(value.X3, value.Y3);
                }
                else throw new Exception("The polygon is to small for a triangle.");
            }
        }

        public Square Square
        {
            get
            {
                if (Points.GetLength(0) >= 4)
                {
                    return new Square(
                        Points[0].X, Points[0].Y,
                        Points[1].X, Points[1].Y,
                        Points[2].X, Points[3].Y,
                        Points[3].X, Points[3].Y
                        );
                }
                else throw new Exception("The polygon is to small for a square.");
            }
            set
            {
                if (Points.GetLength(0) >= 4)
                {
                    Points[0] = new Tuple2D(value.X1, value.Y1);
                    Points[1] = new Tuple2D(value.X2, value.Y2);
                    Points[2] = new Tuple2D(value.X3, value.Y3);
                    Points[3] = new Tuple2D(value.X4, value.Y4);
                }
                else throw new Exception("The polygon is to small for a square.");
            }
        }
    }

    public struct Line
    {
        public int X1, Y1, X2, Y2;

        public Line(int x1, int y1, int x2, int y2)
        {
            this.X1 = x1;
            this.Y1 = y1;
            this.X2 = x2;
            this.Y2 = y2;
        }

        public Line(Tuple2D point1, Tuple2D point2)
        {
            this.X1 = point1.X;
            this.Y1 = point1.Y;
            this.X2 = point2.X;
            this.Y2 = point2.Y;
        }

        public Polygon Polygon
        {
            get { return new Polygon(X1, Y1, X2, Y2); }
            set
            {
                X1 = value.Line.X1;
                X2 = value.Line.X2;
            }
        }
    }

    public struct Triangle
    {
        public int X1, Y1, X2, Y2, X3, Y3;

        public Triangle(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            this.X1 = x1;
            this.Y1 = y1;
            this.X2 = x2;
            this.Y2 = y2;
            this.X3 = x3;
            this.Y3 = y3;
        }

        public Triangle(Tuple2D point1, Tuple2D point2, Tuple2D point3)
        {
            this.X1 = point1.X;
            this.Y1 = point1.Y;
            this.X2 = point2.X;
            this.Y2 = point2.Y;
            this.X3 = point3.X;
            this.Y3 = point3.Y;
        }

        public Polygon Polygon
        {
            get { return new Polygon(X1, Y1, X2, Y2, X3, Y3); }
            set
            {
                X1 = value.Triangle.X1;
                X2 = value.Triangle.X2;
                X3 = value.Triangle.X3;
            }
        }
    }

    public struct Square
    {
        public int X1, Y1, X2, Y2, X3, Y3, X4, Y4;

        public Square(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
        {
            this.X1 = x1;
            this.Y1 = y1;
            this.X2 = x2;
            this.Y2 = y2;
            this.X3 = x3;
            this.Y3 = y3;
            this.X4 = x4;
            this.Y4 = y4;
        }

        public Square(Tuple2D point1, Tuple2D point2, Tuple2D point3, Tuple2D point4)
        {
            this.X1 = point1.X;
            this.Y1 = point1.Y;
            this.X2 = point2.X;
            this.Y2 = point2.Y;
            this.X3 = point3.X;
            this.Y3 = point3.Y;
            this.X4 = point4.X;
            this.Y4 = point4.Y;
        }

        public Square(int x, int y, int width, int height)
        {
            this.X1 = x;
            this.Y1 = y;
            this.X2 = x + width;
            this.Y2 = y;
            this.X3 = x + width;
            this.Y3 = y + height;
            this.X4 = x;
            this.Y4 = y + height;
        }

        public Square(Tuple2D position, int width, int height)
        {
            this.X1 = position.X;
            this.Y1 = position.Y;
            this.X2 = position.X + width;
            this.Y2 = position.Y;
            this.X3 = position.X + width;
            this.Y3 = position.Y + height;
            this.X4 = position.X;
            this.Y4 = position.Y + height;
        }

        public Polygon Polygon
        {
            get { return new Polygon(X1, Y1, X2, Y2, X3, Y3, X4, Y4); }
            set
            {
                X1 = value.Square.X1;
                X2 = value.Square.X2;
                X3 = value.Square.X3;
                X4 = value.Square.X4;
            }
        }
    }

    public struct Round
    {
        public int Radius;
        public int Sides;
        public int X, Y;
        public double Direction;

        public Round(int x, int y, int radius, int sides, double direction)
        {
            this.Radius = radius;
            this.Sides = sides;
            this.X = x;
            this.Y = y;
            this.Direction = direction;
        }

        public Round(Tuple2D position, int radius, int sides, double direction)
        {
            this.Radius = radius;
            this.Sides = sides;
            this.X = position.X;
            this.Y = position.Y;
            this.Direction = direction;
        }
    }

    public struct Circle
    {
        internal Round round;
        public int X { get { return round.X; } set { round.X = value; } }
        public int Y { get { return round.Y; } set { round.Y = value; } }
        public int Radius { get { return round.Radius; } set { round.Radius = value; } }

        public Circle(int x, int y, int radius)
        {
            this.round = new Round(x, y, radius, Math.Max(radius, 360), 0);
        }

        public Circle(Tuple2D position, int radius)
        {
            this.round = new Round(position.X, position.Y, radius, Math.Max(radius, 360), 0);
        }
    }
}
