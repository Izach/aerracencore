﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using OpenGL;

namespace AerracenCore
{
    public static class Draw
    {
        internal static Layer[] layers;
        internal static bool hasInit = false;
        internal static bool hasBegun = false;
        internal static bool hasEnded = false;
        public static int Layer;
        public static Color Color;
        internal static VBO<Vector2> vertexPositions;
        internal static VBO<Vector2> fragmentPositions;

        internal static void Initialize(int maxLayers)
        {
            Gl.Enable(EnableCap.Blend);
            Gl.Disable(EnableCap.DepthTest);
            Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            layers = new Layer[maxLayers];

            for (int i = 0; i < layers.GetLength(0); i++)
            {
                layers[i] = new Layer();
            }

            hasInit = true;
        }

        /// <summary>
        /// Begins the drawing cycle.
        /// </summary>
        public static void Begin()
        {
            if (hasBegun == true) return;

            if (Layer < 0) Layer = 0;
            if (Layer > layers.GetLength(0) - 1) Layer = layers.GetLength(0) - 1;

            Gl.ClearColor(Graphics.ClearColor.R, Graphics.ClearColor.G, Graphics.ClearColor.B, Graphics.ClearColor.A);

            Gl.Viewport(0, 0,
                (int)Window.iWindow.Width,
                (int)Window.iWindow.Height
                );

            Gl.Clear(ClearBufferMask.ColorBufferBit);

            hasBegun = true;
        }

        /// <summary>
        /// Ends the drawing cycle.
        /// </summary>
        public static void End()
        {
            if (hasEnded == true) return;

            for (int i = 0; i < layers.GetLength(0); i++)
            {
                #region Render Polygons
                for (int j = 0; j < layers[i].polygonsToDraw.Count; j++)
                {
                    Polygon polygon = layers[i].polygonsToDraw[j].polygon;
                    Vector2[] points = new Vector2[polygon.Points.GetLength(0)];

                    for (int n = 0; n < polygon.Points.GetLength(0); n++ )
                    {
                        points[n].x = polygon.Points[n].X;
                        points[n].y = polygon.Points[n].Y;
                    }

                    vertexPositions = new VBO<Vector2>(points);

                    Bind(layers[i].polygonsToDraw[j].color, ProgramType.Geometry);

                    Gl.DrawArrays((BeginMode)layers[i].polygonsToDraw[j].mode, 0, vertexPositions.Count);
                }
                #endregion

                #region Render Sprites
                for (int j = 0; j < layers[i].spritesToDraw.Count; j++)
                {
                    Sprite sprite = layers[i].spritesToDraw[j].sprite;

                    vertexPositions = new VBO<Vector2>(new Vector2[] { 
                        sprite.Position.Vector2,
                        new Vector2(sprite.X + sprite.Width, sprite.Y),
                        new Vector2(sprite.X + sprite.Width, sprite.Y + sprite.Height),
                        sprite.Position.Vector2,
                        new Vector2(sprite.X, sprite.Y + sprite.Height),
                        new Vector2(sprite.X + sprite.Width, sprite.Y + sprite.Height)
                        });

                    fragmentPositions = new VBO<Vector2>(new Vector2[] {
                        new Vector2(0, 0),
                        new Vector2(1, 0),
                        new Vector2(1, 1),
                        new Vector2(0, 0),
                        new Vector2(0, 1),
                        new Vector2(1, 1)
                        });

                    Bind(layers[i].spritesToDraw[j].color, ProgramType.Texture, sprite.Texture);

                    Gl.DrawArrays(BeginMode.Triangles, 0, vertexPositions.Count);
                }
                #endregion
            }

            hasEnded = true;
        }

        internal static void Bind(Color color, ProgramType type, Texture texture = null)
        {
            if (type == ProgramType.Geometry)
            {
                Graphics.SetProgram(ProgramType.Geometry);
                Gl.BindBufferToShaderAttribute(vertexPositions, Graphics.GeometryProgram, "vertexPosition");
                Graphics.GeometryProgram["color"].SetValue(color.Vector4);
            }

            if(type == ProgramType.Texture && texture != null)
            {
                Graphics.SetProgram(ProgramType.Texture);
                Gl.BindTexture(texture.iTexture);
                Gl.BindBufferToShaderAttribute(fragmentPositions, Graphics.TextureProgram, "texCoordIn");
                Gl.BindBufferToShaderAttribute(vertexPositions, Graphics.TextureProgram, "vertexPosition");
                Graphics.TextureProgram["color"].SetValue(color.Vector4);
            }
        }

        /// <summary>
        /// Clears the drawing cycle.
        /// </summary>
        public static void Clear()
        {
            if (hasEnded == false) End();

            Window.iWindow.SwapBuffers();


            for (int i = 0; i < layers.GetLength(0); i++)
            {
                layers[i].spritesToDraw.Clear();
                layers[i].polygonsToDraw.Clear();
            }

            hasEnded = false;
            hasBegun = false;
        }

        public static void Line(Line line, Color? color = null, int layer = -1)
        {
            Polygon(new Polygon(line.X1, line.Y1, line.X2, line.Y2), PrimitiveType.Lines, color, layer);
        }

        public static void Line(Tuple2D point1, Tuple2D point2, Color? color = null, int layer = -1)
        {
            Line(new Line(point1, point2), color, layer);
        }

        public static void Line(int x1, int y1, int x2, int y2, Color? color = null, int layer = -1)
        {
            Line(new Line(x1, y1, x2, y2), color, layer);
        }

        public static void Triangle(Triangle triangle, Color? color = null, int layer = -1)
        {
            Polygon(new Polygon(
                new Tuple2D(triangle.X1, triangle.Y1),
                new Tuple2D(triangle.X2, triangle.Y2),
                new Tuple2D(triangle.X3, triangle.Y3)
                ), PrimitiveType.Triangles, color, layer);
        }

        public static void Triangle(Tuple2D point1, Tuple2D point2, Tuple2D point3, Color? color = null, int layer = -1)
        {
            Triangle(new Triangle(point1, point2, point3), color, layer);
        }

        public static void Triangle(int x1, int y1, int x2, int y2, int x3, int y3, Color? color = null, int layer = -1)
        {
            Triangle(new Triangle(x1, y1, x2, y2, x3, y3), color, layer);
        }

        public static void Square(Square square, Color? color = null, int layer = -1)
        {
            Polygon(new Polygon(
                new Tuple2D(square.X1, square.Y1),
                new Tuple2D(square.X2, square.Y2),
                new Tuple2D(square.X3, square.Y3),
                new Tuple2D(square.X3, square.Y3),
                new Tuple2D(square.X4, square.Y4),
                new Tuple2D(square.X1, square.Y1)),
                PrimitiveType.Triangles, color, layer);
        }

        public static void Square(Tuple2D point1, Tuple2D point2, Tuple2D point3, Tuple2D point4, Color? color = null, int layer = -1)
        {
            Square(new Square(point1, point2, point3, point4), color, layer);
        }

        public static void Square(Tuple2D position, int width, int height, Color? color = null, int layer = -1)
        {
            Square(new Square(position, width, height), color, layer);
        }

        public static void Square(int x, int y, int width, int height, Color? color = null, int layer = -1)
        {
            Square(new Square(x, y, width, height), color, layer);
        }

        public static void Square(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, Color? color = null, int layer = -1)
        {
            Square(new Square(x1, y1, x2, y2, x3, y3, x4, y4), color, layer);
        }

        public static void Polygon(Polygon polygon, PrimitiveType mode, Color? color = null, int layer = -1)
        {
            int l = 0;
            Color c;
            if (hasBegun == false) Begin();
            if (layer > -1 && layer <= layers.GetLength(0) - 1) l = layer; else l = Layer;

            if (color.HasValue) c = color.Value;
            else c = Color;

            layers[l].polygonsToDraw.Add(new RPolygon(polygon, mode, c));
        }

        public static void Polygon(PrimitiveType mode, Color? color = null, int layer = -1, params int[] points)
        {
            Polygon(new Polygon(points), mode, color, layer);
        }

        public static void Polygon(PrimitiveType mode, Color? color = null, int layer = -1, params Tuple2D[] points)
        {
            Polygon(new Polygon(points), mode, color, layer);
        }

        public static void Round(Round round, Color? color = null, int layer = -1)
        {
            Tuple2D[] points = new Tuple2D[round.Sides + 2];
            points[0] = new Tuple2D(round.X, round.Y);
            int jump = 360 / round.Sides;

            for(int i = 0; i < round.Sides + 1; i++)
            {
                points[i + 1] = new Vector2D(points[0], round.Radius, (i * jump) + round.Direction ).point2;
            }

            Polygon(new Polygon(points), PrimitiveType.TriangleFan, color, layer);
        }

        public static void Round(int x, int y, int radius, int sides, double direction = 0, Color? color = null, int layer = -1)
        {
            Round(new Round(x, y, radius, sides, direction), color, layer);
        }

        public static void Round(Tuple2D position, int radius, int sides, double direction = 0, Color? color = null, int layer = -1)
        {
            Round(new Round(position, radius, sides, direction), color, layer);
        }

        public static void Circle(Circle circle, Color? color = null, int layer = -1)
        {
            Round(circle.round, color, layer);
        }

        public static void Circle(int x, int y, int radius, Color? color = null, int layer = -1)
        {
            Circle(new Circle(x, y, radius), color, layer);
        }

        public static void Circle(Tuple2D position, int radius, Color? color = null, int layer = -1)
        {
            Circle(new Circle(position, radius), color, layer);
        }

        public static void Sprite(Sprite sprite, Color? color = null, int layer = -1)
        {
            int l = 0;
            Color c;
            if (hasBegun == false) Begin();
            if (layer > -1 && layer <= layers.GetLength(0) - 1) l = layer; else l = Layer;

            if (color.HasValue) c = color.Value;
            else c = Color.White;

            layers[l].spritesToDraw.Add(new RSprite(sprite, c));
        }

        public static void Texture(Tuple2D position, Texture texture, Color? color = null, int layer = -1)
        {
            Sprite(new Sprite(position, texture), color, layer);
        }
    }

    class Layer
    {
        public List<RSprite> spritesToDraw = new List<RSprite>();
        public List<RPolygon> polygonsToDraw = new List<RPolygon>();
    }

    internal class RSprite
    {
        internal Sprite sprite;
        internal Color color;

        public RSprite(Sprite sprite, Color? color)
        {
            this.sprite = sprite;

            if(color.HasValue)
                this.color = color.Value;
        }
    }

    internal class RPolygon
    {
        internal Polygon polygon;
        internal Color color;
        internal PrimitiveType mode;

        public RPolygon(Polygon polygon, PrimitiveType mode, Color color)
        {
            this.polygon = polygon;
            this.color = color;
            this.mode = mode;
        }
    }

    public enum PrimitiveType
    {
        Triangles = BeginMode.Triangles,
        TriangleFan = BeginMode.TriangleFan,
        TriangleStrip = BeginMode.TriangleStrip,
        Lines = BeginMode.Lines,
        LineLoop = BeginMode.LineLoop,
        LineStrip = BeginMode.LineStrip
    }
}
