﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using input = OpenTK.Input;

namespace AerracenCore
{
    public static class Mouse
    {
        public static bool IsClicked(MouseButton button)
        {
            bool success = false;
            if (input.Mouse.GetState().IsButtonDown((input.MouseButton)button))
            {
                success = true;
            }
            else success = false;

            return success;
        }
    }

    public enum MouseButton
    {
        LeftButton = input.MouseButton.Left,
        RightButton = input.MouseButton.Right,
        MiddleButton = input.MouseButton.Middle,
        Button1 = input.MouseButton.Button1,
        Button2 = input.MouseButton.Button2,
        Button3 = input.MouseButton.Button3,
        Button4 = input.MouseButton.Button4,
        Button5 = input.MouseButton.Button5,
        Button6 = input.MouseButton.Button6,
        Button7 = input.MouseButton.Button7,
        Button8 = input.MouseButton.Button8,
        Button9 = input.MouseButton.Button9,
    }
}
