﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerracenCore
{
    public class NTuple
    {
        public int X { get { return N[0]; } set { N[0] = value; } }
        public int Y { get { return N[1]; } set { N[1] = value; } }
        public int Z { get { return N[2]; } set { N[2] = value; } }
        public int W { get { return N[3]; } set { N[3] = value; } }
        public int[] N;

        public NTuple(params int[] n)
        {
            N = n;
        }

        public int Tuple1D()
        {
            return X;
        }

        public Tuple2D Tuple2D()
        {
            return new Tuple2D(X, Y);
        }

        public Tuple3D Tuple3D()
        {
            return new Tuple3D(X, Y, Z);
        }

        public Tuple4D Tuple4D()
        {
            return new Tuple4D(X, Y, Z, W);
        }
    }

    public class NTupleFloat
    {
        public float X { get { return N[0]; } set { N[0] = value; } }
        public float Y { get { return N[1]; } set { N[1] = value; } }
        public float Z { get { return N[2]; } set { N[2] = value; } }
        public float W { get { return N[3]; } set { N[3] = value; } }
        public float[] N;

        public NTupleFloat(params float[] n)
        {
            N = n;
        }

        public float Tuple1DF()
        {
            return X;
        }

        public Tuple2D Tuple2D()
        {
            return new Tuple2D(X, Y);
        }

        public Tuple3D Tuple3DF()
        {
            return new Tuple3D(X, Y, Z);
        }

        public Tuple4D Tuple4D()
        {
            return new Tuple4D(X, Y, Z, W);
        }
    }

    public class Tuple2D
    {
        public int X { get { return (int)x; } set { x = value; } }
        public int Y { get { return (int)y; } set { y = value; } }
        float x, y;

        public Tuple2D(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Tuple2D(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public float FloatX { get { return x; } set { x = value; } }
        public float FloatY { get { return y; } set { y = value; } }

        internal OpenGL.Vector2 Vector2
        {
            get { return new OpenGL.Vector2(X, Y); }
            set { X = (int)value.x; Y = (int)value.y; }
        }
    }

    public class Tuple3D
    {
        public int X { get { return (int)x; } set { x = value; } }
        public int Y { get { return (int)y; } set { y = value; } }
        public int Z { get { return (int)z; } set { z = value; } }

        public Tuple2D Tuple2D
        {
            get { return new Tuple2D(x, y); }
            set { x = value.FloatX; y = value.FloatY; }
        }

        float x, y, z;

        public Tuple3D(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Tuple3D(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public float FloatX { get { return x; } set { x = value; } }
        public float FloatY { get { return y; } set { y = value; } }
        public float FloatZ { get { return z; } set { z = value; } }

        internal OpenGL.Vector3 Vector3
        {
            get { return new OpenGL.Vector3(X, Y, Z); }
            set { X = (int)value.x; Y = (int)value.y; Z = (int)value.z; }
        }
    }

    public class Tuple4D
    {
        public int X { get { return (int)x; } set { x = value; } }
        public int Y { get { return (int)y; } set { y = value; } }
        public int Z { get { return (int)z; } set { z = value; } }
        public int W { get { return (int)w; } set { w = value; } }

        public Tuple2D Tuple2D
        {
            get { return new Tuple2D(x, y); }
            set { x = value.FloatX; y = value.FloatY; }
        }

        public Tuple3D Tuple3D
        {
            get { return new Tuple3D(x, y, z); }
            set { x = value.FloatX; y = value.FloatY; z = value.FloatZ; }
        }

        float x, y, z, w;

        public Tuple4D(int x, int y, int z, int w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public Tuple4D(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public float FloatX { get { return x; } set { x = value; } }
        public float FloatY { get { return y; } set { y = value; } }
        public float FloatZ { get { return z; } set { z = value; } }
        public float FloatW { get { return w; } set { w = value; } }

        internal OpenGL.Vector4 Vector4
        {
            get { return new OpenGL.Vector4(X, Y, Z, W); }
            set { X = (int)value.x; Y = (int)value.y; Z = (int)value.z; W = (int)value.w; }
        }
    }

    public class Vector2D
    {
        public Tuple2D point1;
        public Tuple2D point2;

        public Vector2D(Tuple2D startPosition, int length, double direction)
        {
            point1 = startPosition;
            point2 = new Tuple2D(
                point1.FloatX + (float)Math.Round(Math.Sin(direction * (Math.PI / 180.0)) * length),
                point1.FloatY + (float)Math.Round(Math.Cos(direction * (Math.PI / 180.0)) * length)
                );
        }
    }

    public class Vector3D
    {
        public Tuple3D point1;
        public Tuple3D point2;

        public Vector3D(Tuple3D startPosition, int length, double roll, double pitch, double yaw)
        {
            point1 = startPosition;
            point2 = new Tuple3D(
                point1.FloatX + (float)Math.Round(Math.Sin(yaw * (Math.PI / 180.0)) * length),
                point1.FloatY + (float)Math.Round(Math.Cos(pitch * (Math.PI / 180.0)) * length),
                point1.FloatZ + (float)Math.Round(Math.Tan(roll * (Math.PI / 180.0)) * length)
                );
        }
    }
}
