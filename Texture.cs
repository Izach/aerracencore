﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenGL;

namespace AerracenCore
{
    public class Texture
    {
        internal OpenGL.Texture iTexture;

        public Texture(string filePath)
        {
            iTexture = new OpenGL.Texture(filePath);
        }

        /// <summary>
        /// Disposes of the textures data.
        /// </summary>
        public void Destroy()
        {
            iTexture.Dispose();
        }
    }
}
