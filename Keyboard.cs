﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using input = OpenTK.Input;

namespace AerracenCore
{
    public static class Keyboard
    {
        /// <summary>
        /// Checks if the given key was pressed.
        /// </summary>
        public static bool IsPressed(Key key)
        {
            bool success = false;

            if (input.Keyboard.GetState().IsKeyDown((input.Key)key))
            {
                success = true;
            }
            else success = false;

            return success;
        }
    }

    public enum Key
    {
        A = input.Key.A,
        B = input.Key.B,
        C = input.Key.C,
        D = input.Key.D,
        E = input.Key.E,
        F = input.Key.F,
        G = input.Key.G,
        H = input.Key.H,
        I = input.Key.I,
        J = input.Key.J,
        K = input.Key.K,
        L = input.Key.L,
        M = input.Key.M,
        N = input.Key.N,
        O = input.Key.O,
        P = input.Key.P,
        Q = input.Key.Q,
        R = input.Key.R,
        S = input.Key.S,
        T = input.Key.T,
        U = input.Key.U,
        V = input.Key.V,
        W = input.Key.W,
        X = input.Key.X,
        Y = input.Key.Y,
        Z = input.Key.Z,
        LeftShift = input.Key.ShiftLeft,
        RightShift = input.Key.ShiftRight,
        LeftControl = input.Key.ControlLeft,
        RightControl = input.Key.ControlRight,
        Tab = input.Key.Tab,
        CapitalizationLock = input.Key.CapsLock,
        Tilde = input.Key.Tilde,
        LeftAlternate = input.Key.AltLeft,
        RightAlternate = input.Key.AltRight,
        LeftWindows = input.Key.WinLeft,
        RightWindows = input.Key.WinRight,
        LeftBracket = input.Key.BracketLeft,
        RightBracket = input.Key.BracketRight,
        BackSlash = input.Key.BackSlash,
        ForwardSlash = input.Key.Slash,
        Zero = input.Key.Number0,
        One = input.Key.Number1,
        Two = input.Key.Number2,
        Three = input.Key.Number3,
        Four = input.Key.Number4,
        Five = input.Key.Number5,
        Six = input.Key.Number6,
        Seven = input.Key.Number7,
        Eight = input.Key.Number8,
        Nine = input.Key.Number9,
        Enter = input.Key.Enter,
        Menu = input.Key.Menu,
        Up = input.Key.Up,
        Down = input.Key.Down,
        Left = input.Key.Left,
        Right = input.Key.Right,
        Period = input.Key.Period,
        Escape = input.Key.Escape
    }
}
