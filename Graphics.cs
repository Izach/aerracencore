﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenGL;

namespace AerracenCore
{
    public static class Graphics
    {
        public static Color ClearColor = Color.Black;
        //public static float Scale = 1;
        internal static bool hasInit = false;
        internal static ShaderProgram TextureProgram;
        internal static ShaderProgram GeometryProgram;

        public static class Origin
        {
            public static int X, Y;
            public static Tuple2D Position { set { X = value.X; Y = value.Y; } }

            public static void Set(Alignment align)
            {
                X = Align.GetX(align, Window.iWindow.Width);
                Y = Align.GetY(align, Window.iWindow.Height);
            }
        }

        /// <summary>
        /// Initializes the graphics systems internals.
        /// </summary>
        public static void Initialize(int maxLayers = 100)
        {
            if (Draw.hasInit == false)
            {
                Draw.Initialize(maxLayers);

                #region Geometry Shader Program
                GeometryProgram = new ShaderProgram(@"
                    #version 130

                    in vec2 vertexPosition;

                    uniform mat4 projectionMatrix;

                    void main(void)
                    {
                        gl_Position = projectionMatrix * vec4(vertexPosition, 0, 1);
                    }

                    ", @"

                    #version 130

                    uniform vec4 color;

                    void main(void)
                    {
                        gl_FragColor = color;
                    }

                    ");
                #endregion
                GeometryProgram.Use();
                GeometryProgram["projectionMatrix"].SetValue(
                    Matrix4.CreateOrthographic(Window.iWindow.Width, Window.iWindow.Height, 0, 1));

                #region Texture Shader Program
                TextureProgram = new ShaderProgram(@"
                    #version 130

                    in vec2 vertexPosition;
                    in vec2 texCoordIn;

                    uniform mat4 projectionMatrix;

                    out vec2 texCoord;

                    void main(void)
                    {
                        texCoord = texCoordIn;
                        gl_Position = projectionMatrix * vec4(vertexPosition, 0, 1);
                    }

                    ", @"

                    #version 130

                    in vec2 texCoord;

                    uniform vec4 color;
                    uniform sampler2D textureSampler;

                    void main(void)
                    {
                        gl_FragColor = texture(textureSampler, texCoord) * color;
                    }

                    ");
                #endregion
                TextureProgram.Use();
                TextureProgram["projectionMatrix"].SetValue(
                    Matrix4.CreateOrthographic(Window.iWindow.Width, Window.iWindow.Height, 0, 1));

                hasInit = true;
            }
        }

        internal static void SetProgram(ProgramType type)
        {
            switch (type)
            {
                case ProgramType.Geometry:
                    GeometryProgram.Use();
                    break;

                case ProgramType.Texture:
                    TextureProgram.Use();
                    break;
            }
        }

        /// <summary>
        /// Gets any OpenGL errors, the given code is added to the string.
        /// </summary>
        public static string GetError(int id = -1)
        {
            ErrorCode error = Gl.GetError();
            if (id > -1) return "\"" + error + "\" [" + id + "]";
            else return "\"" + error + "\"";
        }
    }

    internal enum ProgramType
    {
        Geometry,
        Texture
    }

    public enum Alignment
    {
        Top,
        TopLeft,
        TopRight,
        Bottom,
        BottomLeft,
        BottomRight,
        Left,
        LeftTop,
        LeftBottom,
        Right,
        RightTop,
        RightBottom,
        Center,
    }

    public static class Align
    {
        public static int GetX(Alignment align, int width)
        {
            switch(align)
            {
                case Alignment.Bottom: return width / 2;
                case Alignment.BottomLeft: return 0;
                case Alignment.BottomRight: return width;
                case Alignment.Top: return width / 2;
                case Alignment.TopLeft: return 0;
                case Alignment.TopRight: return width;
                case Alignment.Left: return 0;
                case Alignment.LeftBottom: return 0;
                case Alignment.LeftTop: return 0;
                case Alignment.Right: return width;
                case Alignment.RightBottom: return width;
                case Alignment.RightTop: return width;
                case Alignment.Center: return width / 2;
                default: return -1;
            }
        }

        public static int GetY(Alignment align, int height)
        {
            switch (align)
            {
                case Alignment.Bottom: return 0;
                case Alignment.BottomLeft: return 0;
                case Alignment.BottomRight: return 0;
                case Alignment.Top: return height;
                case Alignment.TopLeft: return height;
                case Alignment.TopRight: return height;
                case Alignment.Left: return height / 2;
                case Alignment.LeftBottom: return 0;
                case Alignment.LeftTop: return height;
                case Alignment.Right: return height / 2;
                case Alignment.RightBottom: return 0;
                case Alignment.RightTop: return height;
                case Alignment.Center: return height / 2;
                default: return -1;
            }
        }

        public static Tuple2D GetPosition(Alignment align, int width, int height)
        {
            return new Tuple2D(GetX(align, width), GetY(align, height));
        }
    }
}
