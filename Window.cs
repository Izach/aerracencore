﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.ComponentModel;
using System.Drawing;

namespace AerracenCore
{
    public static class Window
    {
        internal static GameWindow iWindow = new GameWindow();
        public static string Title { get { return iWindow.Title; } set { iWindow.Title = value; } }
        public static int X { get { return iWindow.X; } set { iWindow.X = value; } }
        public static int Y { get { return iWindow.Y; } set { iWindow.Y = value; } }
        public static Tuple2D Position { get { return new Tuple2D(iWindow.X, iWindow.Y); } set { iWindow.X = value.X; iWindow.Y = value.Y; } }
        public static int Width { get { return iWindow.Width; } set { iWindow.Width = value; } }
        public static int Height { get { return iWindow.Height; } set { iWindow.Height = value; } }
        internal static bool hasOpened = false;

        public static void Close()
        {
            iWindow.Close();
        }

        public static void Open()
        {
            if (hasOpened == false)
            {
                if (iWindow.Title == "OpenTK Game Window")
                    iWindow.Title = "Aerracen Core";

                hasOpened = true;
                iWindow.Closing += OnClosing;
                iWindow.Visible = true;

                if (Graphics.hasInit == false) Graphics.Initialize();
            }
        }

        static void OnClosing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }

        /// <summary>
        /// Updates the window, and opens the window, if it has not been already opened.
        /// </summary>
        public static void Update()
        {
            if(hasOpened == false)
                Open();

            iWindow.ProcessEvents();
        }

        private static int lastTick;
        private static int lastFrameRate;
        private static int frameRate;

        public static int GetFPS()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }

            frameRate++;

            return lastFrameRate;
        }
    }
}
